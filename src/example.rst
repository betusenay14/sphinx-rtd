Example Page
==================

Header
------
Math with LaTeX syntax

.. math::
	r_{g,0} = [x, y, z]^T + R_x(\phi) R_y(\theta) R_z(\psi) [0, 0, -d_g]^T
	
Figure
	
.. figure:: fig/uiabot2.png



