# Host URL
Follow the steps below:
![alt text](src/fig/Step0_1.png " ")
![alt text](src/fig/Step0_2.png " ")
![alt text](src/fig/Step1.png " ")
![alt text](src/fig/Step2.png " ")
![alt text](src/fig/Step3.png " ")
![alt text](src/fig/Step4.png " ")
![alt text](src/fig/Step5.png " ")
![alt text](src/fig/Step6.png " ")
![alt text](src/fig/Step7.png " ")
![alt text](src/fig/Step8.png " ")
![alt text](src/fig/Step9.png " ")

# Python packages
Install all required packages using:

`pip install -r requirements.txt`

# Host locally
`sphinx-autobuild . _build/html`

# Sphinx RTD documentation
https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html

# reStructuredText 
https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
